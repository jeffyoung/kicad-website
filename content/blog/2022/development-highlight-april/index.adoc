+++
title = "Development Highlight: April Update"
date = "2022-04-02"
draft = false
"blog/categories" = [
    "Development Highlight"
]
author = "mroszko"
+++

It has been two full months since the February development highlight went over the changes in January.

Here's a fresh update on new features and changes that have happened to 6.99 nightlies. This isn't a complete list
as many changes come in smaller incremental changes that are hard to talk about.

<!--more-->

== Sentry data collection implementation

https://gitlab.com/mroszko[Mark Roszko] introduced the usage of sentry in https://gitlab.com/kicad/code/kicad/-/merge_requests/1166[MR #1166]

Sentry https://sentry.io/ is an open source platform to capture application
events, crash dumps, and other random analytics.

Both their hosted platform (https://github.com/getsentry/sentry) is open
source and the integrated SDK to handle crash handling in C++ is also open
source on GitHub under the MIT license (
https://github.com/getsentry/sentry-native)

Sentry has given KiCad a sponsored account to use their SaaS platform as
the KiCad team does not have the manpower to maintain a self-hosted
instance.


The plan going forward is:

- The primary use in KiCad will be to capture crash reports, but in the
future it could also let us gather things like performance metrics of
algorithms across the wider user base.
- The implementation will be and will remain entirely *opt-in *with a one
time prompt on startup, and the ability to turn it off in preferences. This
is to maintain privacy regulation compliance and just good manners.
- No PII is captured, we have no need or desire for it. Only a random GUID
generated for your install is used to connect crash reports. This GUID can
be reset at any time by the user.
- To aid in avoiding PII capture, all sentry reports go to a "sentry-relay"
server run on KiCad owned infrastructure,
this relay serves as a proxy to communicate to the main sentry.io platform,
this aids in hiding originating IP addresses but we have also turned off
the storage of IP addresses in sentry.
- No user design files are ever uploaded.
- Additionally, the cmake variable KICAD_USE_SENTRY is being added where a
value of false will disable the inclusion of the sentry sdk entirely. The
current default will be off as only Windows support is tested.
- Sentry will only store at most 1 raw crash dump (though we can turn it
off entirely) for analysis. Sentry will parse out only the relevant crash
details such as call stack, and stack frame and discard the rest of the
crash dump.
- Data in sentry is retained for at max 90 days before it is deleted
automatically.
- This will be deployed for Windows nightly builds first


The introduction of sentry resolves a very long term desire of the KiCad team to capture crash dumps without user involvement 
and the use of sentry offers us a very easy solution to do it and help make KiCad better for everyone.


== Orthogonal Dragging
https://gitlab.com/mikebwilliams[Mike Williams'] merge request https://gitlab.com/kicad/code/kicad/-/merge_requests/928[#928] 
to add orthogonal dragging was finally completed and accepted into the codebase.

What is orthogonal dragging? Well in 6.0 and earlier, a drag operation in the Schematic Editor moves symbols
while keeping them attached to the symbol. However, it was a literal form of "attach" and wires would end up
at odd angles and directions.

An example can be seen here of the old behavior:

image::v6-drag.gif[align=center, alt="Old basic dragging", link=v6-drag.gif]


Orthogonal dragging forces all wires to extend and move forming right angles as expected for a schematic.

The new behavior is demonstrated in this animation:

image::v7-drag.gif[align=center, alt="New orthogonal dragging", link=v7-drag.gif]


== Symbol Editor Pin Table Enhancements
https://gitlab.com/kevlan[Kevin Lannen] in https://gitlab.com/kicad/code/kicad/-/merge_requests/813[MR #813] contributed
a few refinements they found necessary to optimize their workflow with large symbols and the KiCad team agreed with the increase
in editing capability.

The additions were:
- Filter pins to a unit
- Change unit of a pin from the table
- Pins can be created or removed in a group symbol by adding/removing the pin number
- A count of number of grouped pins is now present


image::v7-pin-table.png[align=center, alt="Symbol Editor Pin Table", link=v7-pin-table.jpg]

== Off grid ERC warnings
https://gitlab.com/jeffyoung[Jeff Young] in commit https://gitlab.com/kicad/code/kicad/-/commit/41c0009c51b79da517a71cbe4da521b9cc8965d7[41c0009]
added a new ERC check to warn you when you place a symbol with a incompatible grid. A grid mismatch can lead
to situations where connections are not truly made and with a cascade of other issues such as pins being passive
can lead to scenarios where you end up producing bad PCBs.

image::grid-erc.png[align=center, alt="New Off Grid ERC Warning Example", link=grid-erc.png]

== Wires at 45 degree angles
Another wire related contribution by https://gitlab.com/mikebwilliams[Mike Williams] in https://gitlab.com/kicad/code/kicad/-/merge_requests/1146[MR#1146]


KiCad 6.0 originally included a single toggle between
fixed right angle wires and "any angle" mode.

Mike Williams' MR introduces a wire mode for 45 degree
start and end to help in drawing crisp schematics.

You can start drawing a wire and hit the current key combination of `Shift + Spacebar` to cycle between
wire drawing modes.

Placeholder toolbar icons on the left hand side are present to indicate
the current mode. They will eventually receive real icons.


image::wire-mode.gif[align=center, alt="New wire mode shifting example", link=wire-mode.gif]


== Inverse text objects in PCB Editing
Added by https://gitlab.com/jeffyoung[Jeff Young] in https://gitlab.com/kicad/code/kicad/-/commit/293021c58c63d93fc7a2cd5adc9cb9caa2994b82[293021c]

You can now declare a text object as "Knockout" meaning the 
text will subtract from a shaded box instead. This is useful
for making more attention grabbing silkscreen.

image::knockout-text.png[align=center, alt="Symbol Editor Pin Table", link=knockout-text.png]

== Continued Custom Font support

Since the introduction of custom font support in January, numerous issues were fixed. 
As of https://gitlab.com/kicad/code/kicad/-/commit/b55bda8d6fcaefb9b479935f1807af2dcae56d8d[b55bda], custom fonts may now be
used in drawing sheets (formerly page templates).
The initial implementation back in January only introduced support in the Schematic Editor and PCB Editor.


== Automatic Zone Filling

Introduced in https://gitlab.com/kicad/code/kicad/-/commit/d465eb64253bed0d91e49020902507e3fa7d078c[d465eb64] by https://gitlab.com/jeffyoung[Jeff Young], there is now a new
option that will automatically refill zones on a detected change rather than requiring the user to trigger the Zone fill manually.

This is currently off by default as zone filling performance
varies depending on each user's workstation and it may be a hinderance rather than an aid to automatically zone fill.


image::auto-zone-option.png[align=center, alt="Auto zone fill option location indicated in screenshot", link=auto-zone-option.png]


== Continued 3Dconnexion SpaceMouse Support
https://gitlab.com/markus-bonk[Markus Bonk] came back to add SpaceMouse support to the Schematic Editor 
which was merged near the end of March in https://gitlab.com/kicad/code/kicad/-/merge_requests/1169[MR #1169]. 
Previously Space Mouse support was available only in the 3D Viewer and PCB Editor. 

Support for macOS is pending fixes by 3Dconnexion in their macOS drivers.
