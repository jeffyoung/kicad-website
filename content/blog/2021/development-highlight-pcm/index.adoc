+++
title = "Development Highlight: Third Party Content Improvements"
date = "2021-12-04"
draft = false
"blog/categories" = [
    "Development Highlight"
]
author = "mroszko"
+++

We are fast approaching the eventual release of 6.0 targeted for very early next year with the tagging of the 6.0 RC 1.

== Plugin & Content Manager
One final new feature that has been snuck in is the new Plugin & Content Manager (PCM) contributed by https://gitlab.com/qu1ck[Andrew Lutsenko] in link:https://gitlab.com/kicad/code/kicad/-/merge_requests/841[MR #841]

The KiCad community has created an array of valuable python tools and libraries over the years. The PCM now provides the greater KiCad audience an easier way
to both learn of these tools and install them with more ease. 

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="pcm-demo.webm" type="video/webm">
        <source src="pcm-demo.mp4" type="video/mp4">
    </video>
</div>
++++


As of right now the following content is supported:

- Python plugins
- Color themes
- Libraries

By default all users receive a feed of content from the *KiCad official repository* but support exists for third party's to run their own content repositories which users can add.

Unfortunately, there is currently no automatic update or notification functionality. Plugins that require pip packages need the pip packages installed manually by the user.

These are issues we hope to solve in the future beyond the 6.0 release but the current PCM iteration lays down great ground work for future expansion.release.

=== Adding your content to the KiCad official repository

Got a plugin, library or theme you created and want to share? Find out how to link:https://dev-docs.kicad.org/en/addons/[submit it to the kicad official repository]

== Python / Pip Package Access Improvement on Windows

https://gitlab.com/mroszko[Mark Roszko] in a link:https://gitlab.com/kicad/packaging/kicad-win-builder/-/commit/0c0b70b4042688941000337f85f68b4ba91c1b2b[recent change] to Windows packages has added a new *KiCad Command Prompt* entry to the Windows start menu post-install. 
This is coupled with customization of python to fix the heavy handed path defaults that Python is hardcoded to perform on Windows regardless of the fact we are embedding it. 

The `PATH` variable is adjusted in the *KiCad Command Prompt* specifically to prioritize KiCad's distributed `python` and `pip` over any system install you may have.
pip packages now install to `C:\Users\<username>\Documents\KiCad\6.0\3rdparty\Python39\site-packages` to match the new PCM 

image::start-menu.png[align=center, alt="Example of KiCad Command Prompt entry in Start Menu", link=start-menu.png]

image::cmd.png[align=center, alt="Example of KiCad Command Prompt open", link=cmd.png]